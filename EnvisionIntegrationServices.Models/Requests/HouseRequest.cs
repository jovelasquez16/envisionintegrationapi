﻿using Newtonsoft.Json;

namespace EnvisionIntegrationServices.Models.Requests
{
    /// <summary>   A house request. </summary>
    public class HouseRequest : BaseRequest
    {
        /// <summary>   Gets or sets the companycode. </summary>
        ///
        /// <value> The companycode. </value>

        [JsonProperty("companycode")]
        public long Companycode { get; set; }

        /// <summary>   Gets or sets the developmentcode. </summary>
        ///
        /// <value> The developmentcode. </value>

        [JsonProperty("developmentcode")]
        public string Developmentcode { get; set; }

        /// <summary>   Gets or sets the housenumber. </summary>
        ///
        /// <value> The housenumber. </value>

        [JsonProperty("housenumber")]
        public string Housenumber { get; set; }

        /// <summary>   Gets or sets the modelcode. </summary>
        ///
        /// <value> The modelcode. </value>

        [JsonProperty("modelcode")]
        public string Modelcode { get; set; }

        /// <summary>   Gets or sets the elevationcode. </summary>
        ///
        /// <value> The elevationcode. </value>

        [JsonProperty("elevationcode")]
        public string Elevationcode { get; set; }

        /// <summary>   Gets or sets the remarks. </summary>
        ///
        /// <value> The remarks. </value>

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        /// <summary>   Gets or sets the blocknumber. </summary>
        ///
        /// <value> The blocknumber. </value>

        [JsonProperty("blocknumber")]
        public long Blocknumber { get; set; }

        /// <summary>   Gets or sets the lotnumber. </summary>
        ///
        /// <value> The lotnumber. </value>

        [JsonProperty("lotnumber")]
        public long Lotnumber { get; set; }

        /// <summary>   Gets or sets the comments. </summary>
        ///
        /// <value> The comments. </value>

        [JsonProperty("comments")]
        public string Comments { get; set; }

        /// <summary>   Gets or sets the release date. </summary>
        ///
        /// <value> The release date. </value>

        [JsonProperty("release_date")]
        public string ReleaseDate { get; set; }

        /// <summary>   Gets or sets the jionumber. </summary>
        ///
        /// <value> The jionumber. </value>

        [JsonProperty("jionumber")]
        public string Jionumber { get; set; }

        /// <summary>   Gets or sets the currentjobstart. </summary>
        ///
        /// <value> The currentjobstart. </value>

        [JsonProperty("currentjobstart")]
        public string Currentjobstart { get; set; }

        /// <summary>   Gets or sets the lastjobstart. </summary>
        ///
        /// <value> The lastjobstart. </value>

        [JsonProperty("lastjobstart")]
        public string Lastjobstart { get; set; }

        /// <summary>   Gets or sets the buyername. </summary>
        ///
        /// <value> The buyername. </value>

        [JsonProperty("buyername")]
        public string Buyername { get; set; }

        /// <summary>   Gets or sets the settlement date. </summary>
        ///
        /// <value> The settlement date. </value>

        [JsonProperty("settlement_date")]
        public string SettlementDate { get; set; }

        /// <summary>   Gets or sets the deposit date. </summary>
        ///
        /// <value> The deposit date. </value>

        [JsonProperty("deposit_date")]
        public string DepositDate { get; set; }

        /// <summary>   Gets or sets the misc 1 date. </summary>
        ///
        /// <value> The misc 1 date. </value>

        [JsonProperty("misc1_date")]
        public string Misc1Date { get; set; }

        /// <summary>   Gets or sets the datum unused 2. </summary>
        ///
        /// <value> The datum unused 2. </value>

        [JsonProperty("unused_2")]
        public string DatumUnused2 { get; set; }

        /// <summary>   Gets or sets the type of the financing. </summary>
        ///
        /// <value> The type of the financing. </value>

        [JsonProperty("financing_type")]
        public string FinancingType { get; set; }

        /// <summary>   Gets or sets the misc 2 date. </summary>
        ///
        /// <value> The misc 2 date. </value>

        [JsonProperty("misc2_date")]
        public string Misc2Date { get; set; }

        /// <summary>   Gets or sets the misc 3 date. </summary>
        ///
        /// <value> The misc 3 date. </value>

        [JsonProperty("misc3_date")]
        public string Misc3Date { get; set; }

        /// <summary>   Gets or sets the costflag. </summary>
        ///
        /// <value> The costflag. </value>

        [JsonProperty("costflag")]
        public string Costflag { get; set; }

        /// <summary>   Gets or sets the salesreleasedate. </summary>
        ///
        /// <value> The salesreleasedate. </value>

        [JsonProperty("salesreleasedate")]
        public string Salesreleasedate { get; set; }

        /// <summary>   Gets or sets the contract date. </summary>
        ///
        /// <value> The contract date. </value>

        [JsonProperty("contract_date")]
        public string ContractDate { get; set; }

        /// <summary>   Gets or sets the ratified date. </summary>
        ///
        /// <value> The ratified date. </value>

        [JsonProperty("ratified_date")]
        public string RatifiedDate { get; set; }

        /// <summary>   Gets or sets the building number. </summary>
        ///
        /// <value> The building number. </value>

        [JsonProperty("building_num")]
        public string BuildingNum { get; set; }

        /// <summary>   Gets or sets the cntrk submt date. </summary>
        ///
        /// <value> The cntrk submt date. </value>

        [JsonProperty("cntrk_submt_date")]
        public string CntrkSubmtDate { get; set; }

        /// <summary>   Gets or sets the homephone. </summary>
        ///
        /// <value> The homephone. </value>

        [JsonProperty("homephone")]
        public string Homephone { get; set; }

        /// <summary>   Gets or sets the workphone. </summary>
        ///
        /// <value> The workphone. </value>

        [JsonProperty("workphone")]
        public string Workphone { get; set; }

        /// <summary>   Gets or sets the option incv amount. </summary>
        ///
        /// <value> The option incv amount. </value>

        [JsonProperty("option_incv_amt")]
        public long OptionIncvAmt { get; set; }

        /// <summary>   Gets or sets the closing incv amount. </summary>
        ///
        /// <value> The closing incv amount. </value>

        [JsonProperty("closing_incv_amt")]
        public long ClosingIncvAmt { get; set; }

        /// <summary>   Gets or sets the points incv amount. </summary>
        ///
        /// <value> The points incv amount. </value>

        [JsonProperty("points_incv_amt")]
        public long PointsIncvAmt { get; set; }

        /// <summary>   Gets or sets the cooperative amount. </summary>
        ///
        /// <value> The cooperative amount. </value>

        [JsonProperty("coop_amount")]
        public long CoopAmount { get; set; }

        /// <summary>   Gets or sets the permitnumber. </summary>
        ///
        /// <value> The permitnumber. </value>

        [JsonProperty("permitnumber")]
        public string Permitnumber { get; set; }

        /// <summary>   Gets or sets the cooperative yn. </summary>
        ///
        /// <value> The cooperative yn. </value>

        [JsonProperty("coop_yn")]
        public string CoopYn { get; set; }

        /// <summary>   Gets or sets the unused 3. </summary>
        ///
        /// <value> The unused 3. </value>

        [JsonProperty("unused_3")]
        public string Unused3 { get; set; }

        /// <summary>   Gets or sets the orientation. </summary>
        ///
        /// <value> The orientation. </value>

        [JsonProperty("orientation")]
        public string Orientation { get; set; }

        /// <summary>   Gets or sets the loan number. </summary>
        ///
        /// <value> The loan number. </value>

        [JsonProperty("loan_num")]
        public string LoanNum { get; set; }

        /// <summary>   Gets or sets the warrantypolicy. </summary>
        ///
        /// <value> The warrantypolicy. </value>

        [JsonProperty("warrantypolicy")]
        public string Warrantypolicy { get; set; }

        /// <summary>   Gets or sets the address 1. </summary>
        ///
        /// <value> The address 1. </value>

        [JsonProperty("address1")]
        public string Address1 { get; set; }

        /// <summary>   Gets or sets the address 2. </summary>
        ///
        /// <value> The address 2. </value>

        [JsonProperty("address2")]
        public string Address2 { get; set; }

        /// <summary>   Gets or sets the address 3. </summary>
        ///
        /// <value> The address 3. </value>

        [JsonProperty("address3")]
        public string Address3 { get; set; }

        /// <summary>   Gets or sets the warranty date. </summary>
        ///
        /// <value> The warranty date. </value>

        [JsonProperty("warranty_date")]
        public string WarrantyDate { get; set; }

        /// <summary>   Gets or sets the unpackedhousenum. </summary>
        ///
        /// <value> The unpackedhousenum. </value>

        [JsonProperty("unpackedhousenum")]
        public string Unpackedhousenum { get; set; }

        /// <summary>   Gets or sets the misc 4 date. </summary>
        ///
        /// <value> The misc 4 date. </value>

        [JsonProperty("misc4_date")]
        public string Misc4Date { get; set; }

        /// <summary>   Gets or sets the broker amount. </summary>
        ///
        /// <value> The broker amount. </value>

        [JsonProperty("broker_amount")]
        public long BrokerAmount { get; set; }

        /// <summary>   Gets or sets the misc 5 date. </summary>
        ///
        /// <value> The misc 5 date. </value>

        [JsonProperty("misc5_date")]
        public string Misc5Date { get; set; }

        /// <summary>   Gets or sets the misc 6 date. </summary>
        ///
        /// <value> The misc 6 date. </value>

        [JsonProperty("misc6_date")]
        public string Misc6Date { get; set; }

        /// <summary>   Gets or sets the misc 7 date. </summary>
        ///
        /// <value> The misc 7 date. </value>

        [JsonProperty("misc7_date")]
        public string Misc7Date { get; set; }

        /// <summary>   Gets or sets the unused 2. </summary>
        ///
        /// <value> The unused 2. </value>

        [JsonProperty("unused2")]
        public string Unused2 { get; set; }

        /// <summary>   Gets or sets the upgradeprice. </summary>
        ///
        /// <value> The upgradeprice. </value>

        [JsonProperty("upgradeprice")]
        public long Upgradeprice { get; set; }

        /// <summary>   Gets or sets the agentcode. </summary>
        ///
        /// <value> The agentcode. </value>

        [JsonProperty("agentcode")]
        public string Agentcode { get; set; }

        /// <summary>   Gets or sets the brokercode. </summary>
        ///
        /// <value> The brokercode. </value>

        [JsonProperty("brokercode")]
        public string Brokercode { get; set; }

        /// <summary>   Gets or sets the name of the cooperative. </summary>
        ///
        /// <value> The name of the cooperative. </value>

        [JsonProperty("coop_name")]
        public string CoopName { get; set; }

        /// <summary>   Gets or sets the type of the house. </summary>
        ///
        /// <value> The type of the house. </value>

        [JsonProperty("house_type")]
        public string HouseType { get; set; }

        /// <summary>   Gets or sets the broker pct. </summary>
        ///
        /// <value> The broker pct. </value>

        [JsonProperty("broker_pct")]
        public long BrokerPct { get; set; }

        /// <summary>   Gets or sets the list chgord number. </summary>
        ///
        /// <value> The list chgord number. </value>

        [JsonProperty("lst_chgord_num")]
        public string LstChgordNum { get; set; }

        /// <summary>   Gets or sets the misc 8 date. </summary>
        ///
        /// <value> The misc 8 date. </value>

        [JsonProperty("misc8_date")]
        public string Misc8Date { get; set; }

        /// <summary>   Gets or sets the permit date. </summary>
        ///
        /// <value> The permit date. </value>

        [JsonProperty("permit_date")]
        public string PermitDate { get; set; }

        /// <summary>   Gets or sets the pvc 8. </summary>
        ///
        /// <value> The pvc 8. </value>

        [JsonProperty("pvc8")]
        public string Pvc8 { get; set; }

        /// <summary>   Gets or sets the deposit due. </summary>
        ///
        /// <value> The deposit due. </value>

        [JsonProperty("deposit_due")]
        public long DepositDue { get; set; }

        /// <summary>   Gets or sets the estimate base price. </summary>
        ///
        /// <value> The estimate base price. </value>

        [JsonProperty("est_base_price")]
        public long EstBasePrice { get; set; }

        /// <summary>   Gets or sets the estimate options prc. </summary>
        ///
        /// <value> The estimate options prc. </value>

        [JsonProperty("est_options_prc")]
        public long EstOptionsPrc { get; set; }

        /// <summary>   Gets or sets the estimate lot premium. </summary>
        ///
        /// <value> The estimate lot premium. </value>

        [JsonProperty("est_lot_premium")]
        public long EstLotPremium { get; set; }

        /// <summary>   Gets or sets the salesmancode. </summary>
        ///
        /// <value> The salesmancode. </value>

        [JsonProperty("salesmancode")]
        public string Salesmancode { get; set; }

        /// <summary>   Gets or sets the title co. </summary>
        ///
        /// <value> The title co. </value>

        [JsonProperty("title_co")]
        public string TitleCo { get; set; }

        /// <summary>   Gets or sets the meeting prequal date. </summary>
        ///
        /// <value> The meeting prequal date. </value>

        [JsonProperty("mtg_prequal_date")]
        public string MtgPrequalDate { get; set; }

        /// <summary>   Gets or sets the aosacceptedflag. </summary>
        ///
        /// <value> The aosacceptedflag. </value>

        [JsonProperty("aosacceptedflag")]
        public string Aosacceptedflag { get; set; }

        /// <summary>   Gets or sets the estimate upgrade prc. </summary>
        ///
        /// <value> The estimate upgrade prc. </value>

        [JsonProperty("est_upgrade_prc")]
        public long EstUpgradePrc { get; set; }

        /// <summary>   Gets or sets the releasenum. </summary>
        ///
        /// <value> The releasenum. </value>

        [JsonProperty("releasenum")]
        public string Releasenum { get; set; }

        /// <summary>   Gets or sets the cooperative agent address 1. </summary>
        ///
        /// <value> The cooperative agent address 1. </value>

        [JsonProperty("coop_agent_addr1")]
        public string CoopAgentAddr1 { get; set; }

        /// <summary>   Gets or sets the cooperative agent address 2. </summary>
        ///
        /// <value> The cooperative agent address 2. </value>

        [JsonProperty("coop_agent_addr2")]
        public string CoopAgentAddr2 { get; set; }

        /// <summary>   Gets or sets the not used 2. </summary>
        ///
        /// <value> The not used 2. </value>

        [JsonProperty("not_used_2")]
        public string NotUsed2 { get; set; }

        /// <summary>   Gets or sets the estsettl date. </summary>
        ///
        /// <value> The estsettl date. </value>

        [JsonProperty("estsettl_date")]
        public string EstsettlDate { get; set; }

        /// <summary>   Gets or sets the misc 9 date. </summary>
        ///
        /// <value> The misc 9 date. </value>

        [JsonProperty("misc9_date")]
        public string Misc9Date { get; set; }

        /// <summary>   Gets or sets the walk thru date. </summary>
        ///
        /// <value> The walk thru date. </value>

        [JsonProperty("walk_thru_date")]
        public string WalkThruDate { get; set; }

        /// <summary>   Gets or sets the meeting approv date. </summary>
        ///
        /// <value> The meeting approv date. </value>

        [JsonProperty("mtg_approv_date")]
        public string MtgApprovDate { get; set; }

        /// <summary>   Gets or sets the postage. </summary>
        ///
        /// <value> The postage. </value>

        [JsonProperty("postage")]
        public long Postage { get; set; }

        /// <summary>   Gets or sets the walk thru time. </summary>
        ///
        /// <value> The walk thru time. </value>

        [JsonProperty("walk_thru_time")]
        public string WalkThruTime { get; set; }

        /// <summary>   Gets or sets the am pm. </summary>
        ///
        /// <value> The am pm. </value>

        [JsonProperty("am_pm")]
        public string AmPm { get; set; }

        /// <summary>   Gets or sets the unused 4. </summary>
        ///
        /// <value> The unused 4. </value>

        [JsonProperty("unused4")]
        public string Unused4 { get; set; }

        /// <summary>   Gets or sets the conststart date. </summary>
        ///
        /// <value> The conststart date. </value>

        [JsonProperty("conststart_date")]
        public string ConststartDate { get; set; }

        /// <summary>   Gets or sets the lothold. </summary>
        ///
        /// <value> The lothold. </value>

        [JsonProperty("lothold")]
        public string Lothold { get; set; }

        /// <summary>   Gets or sets the meeting applied date. </summary>
        ///
        /// <value> The meeting applied date. </value>

        [JsonProperty("mtg_applied_date")]
        public string MtgAppliedDate { get; set; }

        /// <summary>   Gets or sets the misc 10 date. </summary>
        ///
        /// <value> The misc 10 date. </value>

        [JsonProperty("misc10_date")]
        public string Misc10Date { get; set; }

        /// <summary>   Gets or sets the wostage. </summary>
        ///
        /// <value> The wostage. </value>

        [JsonProperty("wostage")]
        public string Wostage { get; set; }

        /// <summary>   Gets or sets the misc 11 date. </summary>
        ///
        /// <value> The misc 11 date. </value>

        [JsonProperty("misc11_date")]
        public string Misc11Date { get; set; }

        /// <summary>   Gets or sets the misc 12 date. </summary>
        ///
        /// <value> The misc 12 date. </value>

        [JsonProperty("misc12_date")]
        public string Misc12Date { get; set; }

        /// <summary>   Gets or sets the misc 1 field. </summary>
        ///
        /// <value> The misc 1 field. </value>

        [JsonProperty("misc1_field")]
        public string Misc1Field { get; set; }

        /// <summary>   Gets or sets the misc 2 field. </summary>
        ///
        /// <value> The misc 2 field. </value>

        [JsonProperty("misc2_field")]
        public string Misc2Field { get; set; }

        /// <summary>   Gets or sets the buyersname 1. </summary>
        ///
        /// <value> The buyersname 1. </value>

        [JsonProperty("buyersname1")]
        public string Buyersname1 { get; set; }

        /// <summary>   Gets or sets the buyersname 2. </summary>
        ///
        /// <value> The buyersname 2. </value>

        [JsonProperty("buyersname2")]
        public string Buyersname2 { get; set; }

        /// <summary>   Gets or sets the buyersname 3. </summary>
        ///
        /// <value> The buyersname 3. </value>

        [JsonProperty("buyersname3")]
        public string Buyersname3 { get; set; }

        /// <summary>   Gets or sets the previousaddress 1. </summary>
        ///
        /// <value> The previousaddress 1. </value>

        [JsonProperty("previousaddress1")]
        public string Previousaddress1 { get; set; }

        /// <summary>   Gets or sets the previousaddress 2. </summary>
        ///
        /// <value> The previousaddress 2. </value>

        [JsonProperty("previousaddress2")]
        public string Previousaddress2 { get; set; }

        /// <summary>   Gets or sets the promissorynote 1. </summary>
        ///
        /// <value> The promissorynote 1. </value>

        [JsonProperty("promissorynote1")]
        public string Promissorynote1 { get; set; }

        /// <summary>   Gets or sets the promissorynote 2. </summary>
        ///
        /// <value> The promissorynote 2. </value>

        [JsonProperty("promissorynote2")]
        public string Promissorynote2 { get; set; }

        /// <summary>   Gets or sets the promissory 1 date. </summary>
        ///
        /// <value> The promissory 1 date. </value>

        [JsonProperty("promissory1date")]
        public string Promissory1Date { get; set; }

        /// <summary>   Gets or sets the promissory 2 date. </summary>
        ///
        /// <value> The promissory 2 date. </value>

        [JsonProperty("promissory2date")]
        public string Promissory2Date { get; set; }

        /// <summary>   Gets or sets the promissoryamt 1. </summary>
        ///
        /// <value> The promissoryamt 1. </value>

        [JsonProperty("promissoryamt1")]
        public long Promissoryamt1 { get; set; }

        /// <summary>   Gets or sets the promissoryamt 2. </summary>
        ///
        /// <value> The promissoryamt 2. </value>

        [JsonProperty("promissoryamt2")]
        public long Promissoryamt2 { get; set; }

        /// <summary>   Gets or sets the pvc. </summary>
        ///
        /// <value> The pvc. </value>

        [JsonProperty("pvc")]
        public string Pvc { get; set; }

        /// <summary>   Gets or sets the warrantycomments. </summary>
        ///
        /// <value> The warrantycomments. </value>

        [JsonProperty("warrantycomments")]
        public string Warrantycomments { get; set; }

        /// <summary>   Gets or sets the promissorynote 3. </summary>
        ///
        /// <value> The promissorynote 3. </value>

        [JsonProperty("promissorynote3")]
        public string Promissorynote3 { get; set; }

        /// <summary>   Gets or sets the promissory 3 date. </summary>
        ///
        /// <value> The promissory 3 date. </value>

        [JsonProperty("promissory3date")]
        public string Promissory3Date { get; set; }

        /// <summary>   Gets or sets the promissoryamt 3. </summary>
        ///
        /// <value> The promissoryamt 3. </value>

        [JsonProperty("promissoryamt3")]
        public long Promissoryamt3 { get; set; }

        /// <summary>   Gets or sets the depositamtpaid. </summary>
        ///
        /// <value> The depositamtpaid. </value>

        [JsonProperty("depositamtpaid")]
        public string Depositamtpaid { get; set; }

        /// <summary>   Gets or sets the insert 1 date. </summary>
        ///
        /// <value> The insert 1 date. </value>

        [JsonProperty("ins1_date")]
        public string Ins1Date { get; set; }

        /// <summary>   Gets or sets the insert 2 date. </summary>
        ///
        /// <value> The insert 2 date. </value>

        [JsonProperty("ins2_date")]
        public string Ins2Date { get; set; }

        /// <summary>   Gets or sets the insert 3 date. </summary>
        ///
        /// <value> The insert 3 date. </value>

        [JsonProperty("ins3_date")]
        public string Ins3Date { get; set; }

        /// <summary>   Gets or sets the insert 4 date. </summary>
        ///
        /// <value> The insert 4 date. </value>

        [JsonProperty("ins4_date")]
        public string Ins4Date { get; set; }

        /// <summary>   Gets or sets the insert 5 date. </summary>
        ///
        /// <value> The insert 5 date. </value>

        [JsonProperty("ins5_date")]
        public string Ins5Date { get; set; }

        /// <summary>   Gets or sets the pctcompl. </summary>
        ///
        /// <value> The pctcompl. </value>

        [JsonProperty("pctcompl")]
        public string Pctcompl { get; set; }

        /// <summary>   Gets or sets the pvc 1. </summary>
        ///
        /// <value> The pvc 1. </value>

        [JsonProperty("pvc1")]
        public string Pvc1 { get; set; }

        /// <summary>   Gets or sets the casenumber. </summary>
        ///
        /// <value> The casenumber. </value>

        [JsonProperty("casenumber")]
        public string Casenumber { get; set; }

        /// <summary>   Gets or sets the lotcontractdate. </summary>
        ///
        /// <value> The lotcontractdate. </value>

        [JsonProperty("lotcontractdate")]
        public string Lotcontractdate { get; set; }

        /// <summary>   Gets or sets the lotratifydate. </summary>
        ///
        /// <value> The lotratifydate. </value>

        [JsonProperty("lotratifydate")]
        public string Lotratifydate { get; set; }

        /// <summary>   Gets or sets the lotsettledate. </summary>
        ///
        /// <value> The lotsettledate. </value>

        [JsonProperty("lotsettledate")]
        public string Lotsettledate { get; set; }

        /// <summary>   Gets or sets the specflag. </summary>
        ///
        /// <value> The specflag. </value>

        [JsonProperty("specflag")]
        public string Specflag { get; set; }

        /// <summary>   Gets or sets the housetaxenable. </summary>
        ///
        /// <value> The housetaxenable. </value>

        [JsonProperty("housetaxenable")]
        public long Housetaxenable { get; set; }

        /// <summary>   Gets or sets the cellphone. </summary>
        ///
        /// <value> The cellphone. </value>

        [JsonProperty("cellphone")]
        public string Cellphone { get; set; }

        /// <summary>   Gets or sets the email. </summary>
        ///
        /// <value> The email. </value>

        [JsonProperty("email")]
        public string Email { get; set; }

        /// <summary>   Gets or sets the superuserid. </summary>
        ///
        /// <value> The superuserid. </value>

        [JsonProperty("superuserid")]
        public string Superuserid { get; set; }

        /// <summary>   Gets or sets the swornstatement. </summary>
        ///
        /// <value> The swornstatement. </value>

        [JsonProperty("swornstatement")]
        public string Swornstatement { get; set; }

        /// <summary>   Gets or sets the terminator. </summary>
        ///
        /// <value> The terminator. </value>

        [JsonProperty("terminator")]
        public string Terminator { get; set; }

        /// <summary>   Gets or sets the housetaxpercent. </summary>
        ///
        /// <value> The housetaxpercent. </value>

        [JsonProperty("housetaxpercent")]
        public long Housetaxpercent { get; set; }

        /// <summary>   Gets or sets the baseprice. </summary>
        ///
        /// <value> The baseprice. </value>

        [JsonProperty("baseprice")]
        public long Baseprice { get; set; }

        /// <summary>   Gets or sets the optionsprice. </summary>
        ///
        /// <value> The optionsprice. </value>

        [JsonProperty("optionsprice")]
        public long Optionsprice { get; set; }

        /// <summary>   Gets or sets the lotpremium. </summary>
        ///
        /// <value> The lotpremium. </value>

        [JsonProperty("lotpremium")]
        public long Lotpremium { get; set; }

        /// <summary>   Gets or sets the not used 6. </summary>
        ///
        /// <value> The not used 6. </value>

        [JsonProperty("not_used_6")]
        public long NotUsed6 { get; set; }

        /// <summary>   Gets or sets the depositamount. </summary>
        ///
        /// <value> The depositamount. </value>

        [JsonProperty("depositamount")]
        public long Depositamount { get; set; }

        /// <summary>   Gets or sets the mortgage amount. </summary>
        ///
        /// <value> The mortgage amount. </value>

        [JsonProperty("mortgage_amount")]
        public long MortgageAmount { get; set; }

        /// <summary>   Gets or sets the fee pct. </summary>
        ///
        /// <value> The fee pct. </value>

        [JsonProperty("fee_pct")]
        public long FeePct { get; set; }

        /// <summary>   Gets or sets the points code. </summary>
        ///
        /// <value> The points code. </value>

        [JsonProperty("points_code")]
        public long PointsCode { get; set; }

        /// <summary>   Gets or sets the constr loan amount. </summary>
        ///
        /// <value> The constr loan amount. </value>

        [JsonProperty("constr_loan_amt")]
        public long ConstrLoanAmt { get; set; }

        /// <summary>   Gets or sets the pct of blding. </summary>
        ///
        /// <value> The pct of blding. </value>

        [JsonProperty("pct_of_blding")]
        public long PctOfBlding { get; set; }

        /// <summary>   Gets or sets the unique key. </summary>
        ///
        /// <value> The unique key. </value>

        [JsonProperty("unique_key")]
        public string UniqueKey { get; set; }

    }
}
