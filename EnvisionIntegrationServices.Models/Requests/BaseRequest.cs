﻿using Newtonsoft.Json;

namespace EnvisionIntegrationServices.Models.Requests
{
    /// <summary>   A base request. </summary>
    public class BaseRequest
    {
        /// <summary>   Gets or sets the type of the operation. </summary>
        /// <value> The type of the operation. </value>
        [JsonProperty("operation_type")]
        public string OperationType { get; set; }

        /// <summary>   Gets or sets the customer code. </summary>
        /// <value> The customer code. </value>
        [JsonProperty("customer_code")]
        public string CustomerCode { get; set; }

        /// <summary>   Gets or sets the type. </summary>
        /// <value> The type. </value>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>   Gets or sets the unique key. </summary>
        /// <value> The unique key. </value>
        [JsonProperty("unique_key")]
        public string UniqueKey { get; set; }
    }
}
