﻿namespace EnvisionIntegrationServices.Models.Responses
{
    public class HouseResponse
    {
        /// <summary>   Gets or sets the test property. </summary>
        /// <value> The test property. </value>
        public string TestProperty { get; set; }
    }
}
