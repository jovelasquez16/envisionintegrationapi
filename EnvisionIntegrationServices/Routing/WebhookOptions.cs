﻿namespace EnvisionIntegrationServices.Routing
{
    /// <summary> A webhook options. </summary>
    public class WebhookOptions
    {
        /// <summary>   Gets or sets the route prefix. </summary>
        public string RoutePrefix { get; set; } = "webhooks";
    }
}
