﻿using System;
using EnvisionIntegrationServices.Routing;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;

namespace EnvisionIntegrationServices
{
    /// <summary> A startup helper. </summary>
    /// <summary>A startup helper.</summary>
    public static class StartupHelper
    {
        /// <summary>
        /// An IServiceCollection extension method that adds the webhooks to 'spaceAction'.
        /// </summary>
        /// <param name="services">     The services to act on. </param>
        /// <param name="spaceAction">  (Optional) The space action. </param>
        /// <returns>   An IServiceCollection. </returns>
        public static IServiceCollection AddWebhooks(this IServiceCollection services, Action<WebhookOptions> spaceAction = null)
        {
            var options = new WebhookOptions();

            services.Configure<RouteOptions>(opt =>
            {
                opt.ConstraintMap.Add("webhookRoutePrefix", typeof(WebhookRoutePrefixConstraint));
            });

            spaceAction?.Invoke(options);
            
            services.AddSingleton(options);

            return services;
        }
    }
}
