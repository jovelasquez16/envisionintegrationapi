﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace EnvisionIntegrationServices.Controllers.Base
{
    /// <summary>   An accepted handler. </summary>
    /// <typeparam name="TRequest"> Type of the request. </typeparam>
    [ApiController]
    [Route("{prefix:webhookRoutePrefix}/[controller]")]
    public abstract class AcceptedHandler<TRequest>
    {
        /// <summary>   (An Action that handles HTTP POST requests) handles the given request. This handler is useful in scenarios where we queue payloads to be processed later. </summary>
        /// <param name="request">  The request. </param>
        /// <returns>   An asynchronous result. </returns>
        [Route("")]
        [Status(HttpStatusCode.Accepted)]
        [HttpPost]
        public abstract Task Handle([FromBody] TRequest request);
    }
}
