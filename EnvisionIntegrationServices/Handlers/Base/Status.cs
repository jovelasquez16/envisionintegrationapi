﻿using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace EnvisionIntegrationServices.Controllers.Base
{
    /// <summary>   A status. </summary>
    public class Status : ActionFilterAttribute
    {
        /// <summary>   The status code. </summary>
        private readonly HttpStatusCode _statusCode;

        /// <summary>   Constructor. </summary>
        /// <param name="statusCode">   The status code. </param>
        public Status(HttpStatusCode statusCode)
        {
            _statusCode = statusCode;
        }

        /// <summary>   Executes the action executed action. </summary>
        /// <param name="context">  The context. </param>
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            context.Result = new StatusCodeResult((int)_statusCode);
        }
    }
}
