﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace EnvisionIntegrationServices.Controllers.Base
{
    /// <summary>   A response handler. </summary>
    /// <typeparam name="TRequest">     Type of the request. </typeparam>
    /// <typeparam name="TResponse">    Type of the response. </typeparam>
    [ApiController]
    [Route("{prefix:webhookRoutePrefix}/[controller]")]
    public abstract class ResponseHandler<TRequest, TResponse>
    {
        /// <summary>   (An Action that handles HTTP POST requests) handles the given request. </summary>
        /// <param name="request">  The request. </param>
        /// <returns>   An asynchronous result that yields a TResponse. </returns>
        [Route("")]
        [HttpPost]
        public abstract Task<TResponse> HandleAsync([FromBody] TRequest request);
    }
}
