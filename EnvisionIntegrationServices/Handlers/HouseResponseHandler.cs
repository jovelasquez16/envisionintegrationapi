﻿using System.Threading.Tasks;
using EnvisionIntegrationServices.Controllers.Base;
using EnvisionIntegrationServices.Models.Requests;
using EnvisionIntegrationServices.Models.Responses;

namespace EnvisionIntegrationServices.Handlers
{
    public class HouseResponseHandler : ResponseHandler<HouseRequest, HouseResponse>
    {
        public override async Task<HouseResponse> HandleAsync(HouseRequest request)
        {
            var response = new HouseResponse();

            await Task.Run(() =>
            {
                response = new HouseResponse
                {
                    TestProperty = $"Process request successfully. Here is your response!"
                };

            }).ConfigureAwait(false);

            return response;
        }
    }
}
